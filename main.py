from fastapi import FastAPI
from random import randrange

import sentry_sdk
from sentry_sdk.integrations.starlette import StarletteIntegration
from sentry_sdk.integrations.fastapi import FastApiIntegration

app = FastAPI()

@app.get("/")
async def root():
    return {"message": "TESTING FAST API"}

@app.get("/random")
async def root():
    return {"random": randrange(100)}

@app.get("/sentry-debug")
async def trigger_error():
    division_by_zero = 1 / 0

