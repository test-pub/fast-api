# Fast-API building test
- Python
- Poetry
- FastAPI
- Sentry-sdk


## The Method of using the base image:


### If chagnes pyproject, poetry.lock (dependencies) 

Then **Full** build [Dockerfile](https://gitlab.com/test-pub/fast-api/-/blob/main/Dockerfile) and -> Create **base image** registry.gitlab.com/test-pub/fast-api:base

---

### If only the code changes:

Then **Lite** build use file [Dockerlite](https://gitlab.com/test-pub/fast-api/-/blob/main/Dockerlite) -> Used **base image** registry.gitlab.com/test-pub/fast-api:base


---

[Gitlab Registory ](https://gitlab.com/test-pub/fast-api/container_registry)



 

## Gitlab CI

Need gitlab-ci.yml add rules for check change pyproject, poetry.lock 
Exmaples

```
docker build:
  script: 
    - echo "Change dependencies -> Docker Full build and create base image"
    - docker build -t registry.gitlab.com/test-pub/fast-api:base .
  rules:
    changes:
      - pyproject.toml
      - poetry.lock 
```

Or the other way around, use Dockerlite only change code files

```
docker build:
  script: 
    - echo "Change code -> Docker Lite build"
    - docker build . 
  rules:
    changes:
      - app/*
      - *.py
```
