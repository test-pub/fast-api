FROM python:3.10.6-slim-buster

RUN apt update && apt upgrade -y && pip install poetry

WORKDIR /app

COPY pyproject* ./
COPY poetry.lock ./

RUN poetry config virtualenvs.create false
RUN poetry install

COPY . .

CMD uvicorn main:app --reload --workers 1 --host 0.0.0.0 --port 8000
